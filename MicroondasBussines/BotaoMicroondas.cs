﻿namespace MicroondasBussines
{
    public class BotaoMicroondas
    {
        public string BOTAOLEGUMES = "Botão Legumes";
        public string BOTAOLIQUIDOS = "Botão Liquidos";
        private string BOTAOINVALIDO = "Botão Invalido";

        public string apertarBotao(string botao)
        {
            if (botao.Equals(BOTAOLEGUMES))
                return botao;

            if (botao.Equals(BOTAOLIQUIDOS))
                return botao;

            return BOTAOINVALIDO;
        }

        public bool confirmarPreparo(string confirmar)
        {
            if (confirmar.Equals("Sim"))
                return true;

            return false;
        }
    }
}
