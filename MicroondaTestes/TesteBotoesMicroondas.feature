﻿#language: pt

Funcionalidade: Utilizando um microondas	

Como um cozinheiro
Eu quero apertar determinados botões do microondas
De modo que cada botão contém uma programação do tempo de preparo

Entra no Escopo:
Confirmar Operação;
Alertar o cozinheiro que falta 1 minutos para finalizar o preparo;

Fora do Escopo:
Quais foram os alimentos mais preparados;
Confirmar se a porta do microondas está fechada;

Cenário: Preparando Legumes
Quando apertar o "Botão Legumes"
E confirma o preparo: "Sim"
Então o microondas deve apresentar o tempo de "10:00"
E quando faltar um minuto o microondas deve alertar "Falta um minuto para o fim do preparo!!!"

Cenário: Preparando Pipoca
Quando apertar o "Botão Pipoca"
E confirma o preparo: "Sim"
Então o microondas deve apresentar o tempo de "2:20"

Cenário: Preparando Líquidos
Quando apertar o "Botão Liquidos"
E confirma o preparo: "Não"
Então o microondas deve apresentar o tempo de "Função não programada"

