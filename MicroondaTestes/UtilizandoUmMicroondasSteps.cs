﻿using System;
using MicroondasBussines;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace MicroondaTestes
{
    [Binding]
    public class UtilizandoUmMicroondasSteps
    {
        private string botaoPrecionado;
        private bool confirmacao;

        [When(@"apertar o ""(.*)""")]
        public void QuandoApertarO(string p0)
        {
            BotaoMicroondas botaoMicro = new BotaoMicroondas();

            botaoPrecionado = botaoMicro.ApertarBotao(p0);
            Assert.AreEqual(botaoPrecionado, p0);
        }

        [When(@"confirma o preparo: ""(.*)""")]
        public void QuandoConfirmaOPreparo(string p0)
        {
            BotaoMicroondas confirmaAcao = new BotaoMicroondas();
            confirmacao = confirmaAcao.ConfirmarPreparo(p0);
        }

        [Then(@"o microondas deve apresentar o tempo de ""(.*)""")]
        public void EntaoOMicroondasDeveApresentarOTempoDe(string p0)
        {
            Preparo preparo = new Preparo();

            var tempo = preparo.tempoPreparo(botaoPrecionado, confirmacao);
            Assert.AreEqual(tempo, p0);
        }

        [Then(@"quando faltar um minuto o microondas deve alertar ""(.*)""")]
        public void EntaoQuandoFaltarUmMinutoOMicroondasDeveAlertar(string p0)
        {
            Temporizador temporizador = new Temporizador();
            var tempo = temporizador.VerificarTempoRestante("1:00");

            Assert.AreEqual(tempo, p0);
        }
    }
}
